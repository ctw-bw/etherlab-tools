function D=ReadDLSData(dirname)
% ReadDLSData  Reads data from an exported DLS directory and returns it as a struct. 
%
% D = ReadDLSData([dirname])
%
% Optionally, you can enter a directory name. If a file dls_export_info
% was found in the directory, it is considered the directory you want to
% read. Otherwise, the given directory name is considered as the base for
% which a directory selection window is given.
%
% Note: not much error checking is done!

% The first part of the .signals-file, up to a line of 80 '=' chars+'\r\n',
% is an ssdf format. The rest is binary 64 bits float data ('double' in
% Matlab)
%
%% Parameters
MAX_UNSYNC = 50; % us; the maximum allowed un-synchronization between channels
                 % there may be a few us difference in the sample times; we allow for 50 us difference here.

%% Find out which directory to use and if it's a valid data directory
DLSDirectoryIdentifier = 'dls_export_info'; % We look for this file name to see whether we have an exported dls directory

if ~exist('dirname','var')
    dirname = 'SomeNoneExistentDir';
end;

type = exist(dirname); %#ok<EXIST>
if type == 2
    % dirname is a file
    error([mfilename ': The given argument is a file, but expected a directory']);
elseif type == 7 || strcmp(dirname,'SomeNoneExistentDir')
    % dirname is a directory. Now check whether we need this directory directly or
    % still show a directory selection window
    if ~exist(fullfile(dirname, DLSDirectoryIdentifier),'file')
    
        newdir = uigetdir(dirname, 'Select exported DLS directory');
        if newdir==0
            % No files was selected
            disp('ReadDLSData canceled.');
            D=[];
            return
        elseif ~exist(fullfile(newdir, DLSDirectoryIdentifier),'file')
            % Directory is not a exported DLS directory
            error([mfilename ': The selected directory does not contain exported DLS data.']);
        end;
        % If we're here, a valid directory was selected
        dirname = newdir;
    end;
else
    error([mfilename ': the specified file does not exist (',dirname,').']); 
end;


%% Now read all data as-is and store it in a temporary struct

disp(['Loading data from ',dirname])
list=sort_nat(dir(fullfile(dirname,'channel*.dat')));
N = length(list); % Number of files to be read
tempData= struct( ...
    'fileName','', ...
    'channelName','', ...
    'channelNameSafe','', ...
    'data',cell(1,N), ...
    'startTime',nan, ...
    'endTime',nan, ...
    'startIndex',nan, ...
    'endIndex',nan, ...
    'dataLength',nan); % Preallocate place to store all data in

for index=1:length(list)  %for all files in the folder
    fprintf(1,'  Loading channel %i of %i\n',index,length(list));
    fileName = fullfile(dirname, list{index});
    tempData(index).fileName = fileName;
    
    % Get the signal name
    fid = fopen(fileName, 'rt'); 
    fgets(fid); fgets(fid); % Skip first two lines
    c = fgets(fid);
    fclose(fid);
    NameStart=regexp(c, '/');                %find the name of the signal
    tempData(index).channelName =strtrim(c(NameStart(1)+1:end));
    tempData(index).channelNameSafe = regexprep(tempData(index).channelName, '[\s\\/]*', '_'); %Replace spaces, \ and / with underscores
    
    % Read the data
    tempData(index).data = load(fileName); % First column is time (us), second is value
    
    if isempty(tempData(index).data)
        % Then this was an 'empty channel'. This can happen if, with DLS,
        % you try to log a signal/channel that doesn't exist anymore.
        % In such a case, we don't add anything, and just continue with the
        % next channel
        fprintf(1,'    %s (%s) has no numerical data (tried to log a non-existent channel?)\n',list{index},tempData(index).channelName);
    else
        tempData(index).startTime = tempData(index).data(1,1);
        tempData(index).endTime = tempData(index).data(end,1);
    end 
end;

%% Extract informatio for reshaping Data
% This part uses all read data to generate extra control variables that
% facilitate generating the final struct later.
disp('Reshaping data');
disp('  Removing empty channels.');
startTime = max( [tempData.startTime]);
endTime = min( [tempData.endTime]);
disp ('  Removing samples for which not all channels have data (from begin and end).')
nonEmptyChannelIndices = find(~isnan([tempData(:).startTime])); % So that we can loop over all non-empty channels

for i=nonEmptyChannelIndices
    % Find indices of the parts of the data that we want to save (i.e.,
    % remove any data for which not all channels have data). We could also
    % remove the unneeded samples right-away, but I think this is much
    % faster.
    startIndex = 1;
    while tempData(i).data(startIndex,1)< startTime-MAX_UNSYNC 
        startIndex = startIndex + 1;
    end;
    if tempData(i).data(startIndex,1)>startTime+MAX_UNSYNC
        fprintf(1,'    Warning: data seems to be not so synchronous (more than 10 us difference between different channels;\n');
        fprintf(1,'      found out when comparing time data of channel %d to the maximum start time)\n',i);
        fprintf(1,'      channel start time: %16i, max start time: %16i; difference: %16i\n',startTime,tempData(i).data(startIndex,1),startTime-tempData(i).data(startIndex,1));
    end;

    endIndex=size(tempData(i).data,1);
    while tempData(i).data(endIndex,1)> endTime+MAX_UNSYNC
        endIndex = endIndex - 1;
    end;
    if tempData(i).data(endIndex,1)<endTime-MAX_UNSYNC
        fprintf(1,'    Warning: data seems to be not so synchronous (more than 10 us difference between different channels;\n');
        fprintf(1,'      found out when comparing time data of channel %d to the minimum end time)\n',i);
        fprintf(1,'      channel end time: %16i, min end time: %16i; difference: %16i\n',endTime,tempData(i).data(endIndex,1),endTime-tempData(i).data(endIndex,1));
    end;
    tempData(i).startIndex = startIndex;
    tempData(i).endIndex = endIndex;
    tempData(i).dataLength = endIndex-startIndex+1;
end;

% Check whether all data is now equally long (it should be)
mn = min([tempData.dataLength]);
mx = max([tempData.dataLength]);
if ( mn ~= mx)
    disp('  Error! Data channels are not same sample time. This is not supported by this function');
    disp('    (neither it is supported by the resulting data structure, since it uses one time axis');
    disp('    for all data combined).');
    fprintf(1, '    Minimum data length was %i; maximum data length was %i (they should be equal...)',mn,mx);
    return;
end;

%% Assign all fields of D
% 

%Pre-allocate
first=nonEmptyChannelIndices(1); % Index of first non-empty channel
D.rawdata = zeros(tempData(first).dataLength, length(nonEmptyChannelIndices)+1); % Pre-allocate; Time is stored as well

% Create names map
D.map = ['time', {tempData(nonEmptyChannelIndices).channelName}];

% Add time column to data
timeColumn = tempData(first).data( tempData(first).startIndex:tempData(first).endIndex,1); % in us
D.startTime_us = timeColumn(1);
D.data.time = (timeColumn - timeColumn(1)) / 1e6; % Now in seconds, starting at t=0
D.rawdata(:,1) = D.data.time;

% Add all channels to data
for i = 1:length(nonEmptyChannelIndices)
    index = nonEmptyChannelIndices(i);
    chn = tempData(index).channelNameSafe; 
    D.data.(chn) = tempData(index).data(tempData(index).startIndex:tempData(index).endIndex,2);
    D.rawdata(:,i+1) = D.data.(chn);
end;

%  Store some other data
D.fileName = dirname;
D.startDate = datestr(D.startTime_us/86400/1e6 + datenum(1970,1,1));
disp('Done loading data');

end

%% Local helper functions
function y=sort_nat(l)
    %Given a list of files names 'channelXXX.dat', with XXX a number,
    % generated by dir(), it returns a cell array of the channels, sorted
    % in a natual order (i.e., channel1, channel2, ... channel10 and not
    % channel1, channel10, ... channel2).
    
    % First get each number out of the list
    ll=cell(length(l),2);
    for i=1:length(l)
        name = l(i).name;
        n = str2double(name(8:(length(name)-3)));
        ll(i,1:2) = [{n}, {name}];
    end;
    
    ll_sort = sortrows(ll);
    y = ll_sort(:,2);   
end