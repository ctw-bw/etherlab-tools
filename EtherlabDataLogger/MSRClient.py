"""MSRClient.py
See docstring on the main class.
"""

import socket
import select
import re
import numpy as np

class Channel(dict):
    """Represents a channel (a Simulink Signal). At this moment, its implementation
    is just a dict, but in the future there may be more stuff added."""
    pass
    
class Parameter(dict):
    """Represents a parameter (a Simulink Parameter). At this moment, its implementation
    is just a dict, but in the future there may be more stuff added."""
    pass
    
class ConnectionLostException(Exception):
    """Exception which is raised when a TCP/IP connection loss is detected."""
    
class ChannelsLogger:
    """Provides a way of logging channels (Simulink Signals). It makes use of the MSR <xsad>/<xsod> commands,
    which starts/stops the automatic sending of data.
    In order to initialize/finalize the communication with the real time process, startLogging/stopLogging should be called.
    During logging, the function poll() should be called periodically. This will fetch all available data from the TCP/IP queue
    and call the function _processData which you can subclass to do whatever you want. The default _processData 
    shows a text message about the data received.
    """
    
    # A ChannelsLogger always has its own TCP/IP connection to the client (i.e., it doesn't share it with the
    # MSRClient thread). This ensures that never two ChannelsLogger instances share the same connection,
    # eliminating the need for hard-to-write detection of which <data> block is for which logger instance.
    
    def __init__(self, MSR, channels = None, blockSize = None, decimation = None, run = False):
        """Initializes the ChannelsLogger.
        If run is True, the logger is started immediately."""
        self._MSR = MSR
        self._blockSize = blockSize
        self._decimation = decimation
        self._isRunning = False
        
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sock.connect(self._MSR.ipPort())
        self._sock.settimeout(1)
        
        # Initialize receive buffer
        self._recvTagBuffer = b'' # For use within _recvtag
        
        # Initialize poll buffer
        self._pollBuffer = '' # For use in poll()
        
        self.setChannels(channels)

        if run:
            self.startLogging()
        
    def __del__(self):
        if self._isRunning:
            print("Warning: Deleting running logger. Trying to do stopLogging before really deleting.")
            self.stopLogging()
            
    def _onConnectionLoss(self, message):
        """This function is called when a TCP/IP connection loss is detected. You can override this
        function in a subclass to define your own behaviour. 
        (Default behaviour is raising a ConnectionLostException)."""
        print("Connection was lost (_onConnectionLoss was called.) Message: \"%s\"."%message)
        raise ConnectionLostException(message)
        
    def _recv(self, blocking = False):
        """Wrapper around self._sock.recv, which makes it (hopefully) more robust against connection loss. If blocking is False
        and no data is available, it returns None"""
        try:
            if not blocking and not self._recvDataAvailable():
                return None
            recvd = self._sock.recv(9999)
        except socket.error as E:
            self._onConnectionLoss("Socket Error! (error: %s)"%E)
        if recvd=="":
            # Then the connection was closed
            self._onConnectionLoss("Connection was closed (empty string received)")
        return recvd
        
    def _recvDataAvailable(self):
        """Returns True if we can read data from the socket (i.e., if a recv-call would not block)."""
        r,w,e = select.select([L._sock],[],[],0)
        return len(r)!=0
        
    def _recvTag(self, blocking = False):
        """Receives one XML tag from the TCP/IP stream. It does NOT merge nested tags into one, so
        <channels>
          <channel index="102" .../>
        </channels>
        needs three calls to this function to be received completely.
        A tag is detected as everthing  between an opening < and closing > (including the delimiters).
        There is no checking if the contents of the tag contains a < or > , but a quick test showed
        that, if I make a signal name containing a > character, it is replaced by &gt; in the tag, so
        I'm quite sure that this will never occur.
        
        Any characters prior to the first < will be discarded right away.
        If no data is available yet, it blocks identically to self._sock.recv.
        
        The tag is returned as a string (not as a binary sequence); if no full tag was available and blocking is False,
        this function returns None."""
        while b'>' not in self._recvTagBuffer:
            recvd = self._recv(blocking)
            if recvd is None:
                return None
            else:
                self._recvTagBuffer +=recvd
            
        startIndex = self._recvTagBuffer.find(b'<')
        endIndex = self._recvTagBuffer.find(b'>')
        if endIndex<startIndex:
            raise RuntimeError("Found closing tag '>' without opening tag '<' in received data ",self._recvTagBuffer)
        
        tag,self._recvTagBuffer = self._recvTagBuffer[startIndex:endIndex+1],self._recvTagBuffer[endIndex+1:]
        return tag.decode()
       
       
    def _readUntilEmpty(self):
        """Reads from the socket until no more data arrives. Basically, it
        clears the input buffer."""
        while self._recv(False) is not None:
            pass
            
#         self._sock.settimeout(0)
#         try:
#             while True:
#                 self._recv(False)
#         except BlockingIOError:
#             pass
#                 
#         self._sock.settimeout(1)
        
    def _hasDuplicates(self,l):
        """Returns True or False, depending on whether the list l has duplicat values"""
        return len(l)!=len(set(l))
        
    def setChannels(self, channels):
        """Given a list of channels (where each element may be either a channel name, channel index
        or an instance of the channel class), this funcion sets up the logger to log these channels.
        
        There may be no duplicates in the channels list.
        If the logger is running, an exception is raised."""
        if self._isRunning:
            raise RuntimeError("Cannot set/change channels when logger is running.")
        
        if channels is None:
            self._channelIndices = []
            return
        
        if not isinstance(channels, (list, tuple)):
            raise TypeError("'channels' should be a list or tuple.")
        
        channelIndices = [self._MSR.channelIndex(c) for c in channels] # The indices
        
        if self._hasDuplicates(channelIndices):
            raise ValueError("The list of channels has duplicates; this is not allowed.")
            
        self._channelIndices = channelIndices
        
        # Prepare a channelIndexToColumnNumber dict (the keys being the
        # channel indices, values being column numbers; this is the
        # opposite of self._channelIndices
        self._channelIndexToColumnNumber =  {v:k for (k,v) in enumerate(self._channelIndices)}
        
        
        
    def startLogging(self):
        """Starts logging. After executing this function, the client automatically sends new data
        to this host."""
        if self._isRunning:
            raise RuntimeError("Logger is already running")
        if len(self._channelIndices) == 0:
            raise RuntimeError("Cannot start a logger which logs 0 channels.")
        
        # Empty TCP/IP queue
        self._readUntilEmpty()
        
        # Generate start command and send it.
        options = ""
        if self._blockSize is not None:
            options = ' blocksize="%i"'%self._blockSize
        if self._decimation is not None:
            options = ' reduction="%i"'%self._decimation
        
        s = '<xsad channels="' + ','.join([str(i) for i in self._channelIndices])+'"'+options+">\r\n"
        self._sock.send(s.encode())
        
        
        
        
        self._isRunning = True
        # For debug
        print(s)
        
    def channelIndexToColumnNumber(self,ch):
        """Given a channel index, this function returns in which column the data of this channel should be."""
        return self._channelIndexToColumnNumber[ch]
        
    def stopLogging(self):
        """Stops logging."""
        if self._isRunning == False:
            raise RuntimeError("Cannot stop a non-running logger")
            
        self._sock.send("<xsod>\r\n".encode())
        self._isRunning = False
        
    def poll(self, blocking = False):
        """This function should be called periodically. It reads all available data from the TCP/IP queue,
        decodes it and adds calls the _processData function with the data as the argument (if there is data). 
        
        You can use this function in two ways: non-blocking (default) and blocking.
        - Non-blocking one makes sense if you integrate it in your main loop or use some
          kind of timer to schedule the polling. This way, if no data is available,
          the function returns immediately.
        - Blocking is useful if this function is the only function executed by a thread
          (while True: X.poll()). Basically, this will cause the data being processed as soon as it has
          arrived.
          
        Note that the implementation in this function heavily depends on the exact strings sent;
        e.g., if attibutes of XML tags are sent in a different order (which is allowed according to XML),
        this function breaks. I prefer this however over doing more checks and more complicated programming
        because it would make this function slower."""
       
        if not self._isRunning:
            # It could occur that poll is called after stopping the logging (mabye because of thread 
            # asynchronicity?). In order to prevent problems, we just do as if poll was not called
            # in those cases
            return
            
        # Read until we've got a full frame
        while True:
            tag = self._recvTag(blocking)
            if tag is None:
                # Then no data was available (and we shouldn't wait for data). So, return without doing anything
                return
            
            if tag.startswith('<data'):
                # New frame; set up things. Get time of last data point (in seconds since 1 Jan 1970)
                self._timeOfLastDataPointInFrame = float(tag.split('"')[3])
                self._dataAccumulator = None
                pass
            elif tag.startswith('<F '):
                temp = tag.split('"')
                channelNumber = int(temp[1])
                    
                if self._dataAccumulator is None:
                    # Then this is the first data tag; initalize the data accumulator
                    data = [float(x) for x in temp[3].split(',')]
                    self._dataAccumulator = np.zeros((len(data), len(self._channelIndices)))
                    self._dataAccumulator[:,self.channelIndexToColumnNumber(channelNumber)] = data
                else:
                    self._dataAccumulator[:,self.channelIndexToColumnNumber(channelNumber)] = [float(x) for x in temp[3].split(',')]
                    
            elif tag == '</data>':
                # Last tag; we now have a full frame so call _processData
                self._processData(self._dataAccumulator)
                
                # Check whether we should continue processing more data. This is a little hard, because it depends on
                # the value of blocking as well as what happened in the past and now:
                # If blocking == False:
                #   We can just continue until there is no more data. If so, we'll leave this function by the return "(if tag is None").
                # If blocking == True:
                #   We need to output at least one frame of data (wait for it if needed) but also all other data which is already
                #   available. This is equivalent to first reading a frame with blocking=True, and any subsequent frames with
                #   blocking=False. Hence, by setting the variable Blocking to false here, we simply accomplish what we want.
                #   Note that, from now, the behaviour is identical to non-blocking, so we will leave this function 
                #   by the return ("if tag is None")
                blocking = False # This affects the self._recvTag() in this while loop
            else:
                # Some unrelated tag (most probably a parameter update notification or welcome string).
                # Ignore and continue
                continue
            
        # We never get here.
     
    def _processData(self, data):
        """The default action to be taken to data: Show a text message"""
        print("Received %d stamples of data (last at time %f)"%(data.shape[0],self._timeOfLastDataPointInFrame))
#         print(data)
     
class MSRClient:
    """A client for the MSR library.
    Supports (when it's finished):
    - Reading channel and parameter information
    - Reading channels with request-answer protocol
    - Parameter interaction (get and set)
    - Scopes/automatic channel logging (<xsad> and <xsod> commands in MSR).
    
    Using the Reequest-answer method for querying a channel (may be a matrix though) takes a little
    more than 0.02 s (so max 50 reads per second...). This is due to the server; it doesn't respond
    earlier than after 0.02 s (however, getting the (long) list of channels is much faster).
    
    Note: 'channel' is the Etherlab term for 'signal' in Simulink.
    """
    MAXLEN = 65535
    
    def __init__(self, ip, port=2345, useNumpy = True, debug = False):
        """Connects to the model and reads all channels."""
        
        self._ip = ip
        self._port = port
        self._useNumpy = useNumpy
        self._debug = debug
        
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._sock.connect((ip,port))
            
        self._sock.settimeout(1)
        self._recvTagBuffer = b''
        # Read header sent by model (and ignore it)
        self._recvTag()
        self._readParameters()
        self._readChannels()
        
        s = """<remote_host name="unknown" applicationname="Python MSRSimpleClient interface" access="allow">"""
        self._sock.send(s.encode())
        
        
    ## General functions (not related to parameters or channels specificly
    def ipPort(self):
        """Returns a tuple of the ip and port numbers"""
        return (self._ip, self._port)
        
    def _convertToNumberIfPossible(self, s):
        """Returns the value of s (either as int or float) if s seems to be representing such,
        or just the string s itself otherwise."""
        try:
            return int(s)
        except ValueError:
            pass # Go on with the next try
        try:
            return float(s)
        except ValueError:
            return s   
        
    def _recvTag(self):
        """Receives one XML tag from the TCP/IP stream. It does NOT merge nested tags into one, so
        <channels>
          <channel index="102" .../>
        </channels>
        needs three calls to this function to be received completely.
        A tag is detected as everthing  between an opening < and closing > (including the delimiters).
        There is no checking if the contents of the tag contains a < or > , but a quick test showed
        that, if I make a signal name containing a > character, it is replaced by &gt; in the tag, so
        I'm quite sure that this will never occur.
        
        Any characters prior to the first < will be discarded right away.
        If no data is available yet, it blocks identically to self._sock.recv.
        
        The tag is returned as a string (not as a binary sequence)."""
        while b'>' not in self._recvTagBuffer:
            recvd = self._sock.recv(self.MAXLEN)
            if recvd==b'':
                raise RuntimeError("Connection was closed.")
            self._recvTagBuffer +=recvd
            
        startIndex = self._recvTagBuffer.find(b'<')
        endIndex = self._recvTagBuffer.find(b'>')
        if endIndex<startIndex:
            raise RuntimeError("Found closing tag '>' without opening tag '<' in received data ",self._recvTagBuffer)
        
        tag,self._recvTagBuffer = self._recvTagBuffer[startIndex:endIndex+1],self._recvTagBuffer[endIndex+1:]
        return tag.decode()
     
    def _valueToString(self, v, channelOrParameter):
        """Given a value v (either a scalar, a list, a list of lists or a numpy array), this function
        creatses a string which can be used to send as an XML argument. Also, this function checks whether
        the format is compatible with the given channel or parameter (if not, a ValueError is raised)."""
        
        def allElementsIsInstance(l, instanceType):
            """Returns true if all elements of l are of type instanceType (which may be a tuple of types)"""
            return all( [ isinstance(e, instanceType) for e in l])
             
        typ = channelOrParameter["typ"] # Don't overwrite the function 'type'
        
        if typ=='TDBL':
            if not isinstance(v,(float, int)):
                raise ValueError('Expected value of type float (TDBL) but received a %s'%type(v))
            return str(v)
        elif typ in ['TUINT','TINT']:
            if not isinstance(v,int):
                raise ValueError('Expected value of type %s but received a %s'%(typ,type(v)))
            if v<0 and typ=='TUINT':
                raise ValueError('Expected an unsigned int (TUINT) but received a negative integer %i'%v)
            return str(v)
            
        elif typ in ['TDBL_LIST', 'TINT_LIST','TUINT_LIST']:
            elementType = (float,int) if typ.startswith('TDBL') else (int)
            if isinstance(v, list):
                if len(v)!= channelOrParameter['anz']:
                    raise ValueError('Expected a list of %i elements but the list contains %i elements'%(channelOrParameter['anz'], len(v)))
                if not allElementsIsInstance(v, elementType):
                    raise ValueError('Expected a list of %s but at least one element was of a different type'%typ[0:4])
                if typ=='TUINT_LIST' and any( [ e<0 for e in v]):
                    raise ValueError('Expected a list of unsigned ints (TUINT) but at least one element was negative')
                return ','.join( [str(e) for e in v])
            elif isinstance(v, np.ndarray):
                raise NotImplementedError('Converting a numpy array to %s is not implemented yet.'%typ)
            else:
                raise ValueError('Expected a list or np.array (%s) but received a %s'%(typ,type(v)))
        elif typ in ['TDBL_MATRIX', 'TINT_MATRIX', 'TUINT_MATRIX']:
            raise NotImplementedError('Converting a matrix is not implemented yet. As a work-around, you can set each column separately (by using <ParameterName>/0 etc.)')
        else:
            raise NotImplementedError('Converting a %s is not implemented yet.'%typ)
            
                
    def _stringToValue(self, s, channelOrParameter, useNumpy=None):
        """Given a string s, containing a value, and an instance of a Channel or Parameter (which holds 
        information about the format of s), this function returns the numerical value of s (a scalar, 
        list, lists of lists or np.array). Depending on the value of self._useNumpy and a possible
        override of useNumpy, a choice is made between lists and np.array."""
        if useNumpy is None:
            useNumpy = self._useNumpy
            
        typ = channelOrParameter["typ"]
        # Scalars
        if typ == 'TDBL':
            return float(s)
        elif typ in ['TINT','TUINT','TUCHAR']: 
            return int(s)
        # Vectors
        elif typ == 'TDBL_LIST': 
            v = [float(v) for v in s.split(',')]
            return np.array(v) if useNumpy else v
        elif typ in ['TINT_LIST', 'TUINT_LIST']: 
            v = [int(v) for v in s.split(',')]
            return np.array(v) if useNumpy else v
        # Matrices
        elif typ in ['TDBL_MATRIX', 'TINT_MATRIX','TUINT_MATRIX']:
            # First check whether we understand the format of the matrix
            if channelOrParameter['orientation']!='MATRIX_ROW_MAJOR':
                raise RuntimeError("Unsupported orientation for %s: %s"%(typ,channelOrParameter['orientation']))

            # Create a list of all values
            if typ.startswith('TDBL'):
                v = [float(v) for v in s.split(',')]
            else:
                v = [int(v) for v in s.split(',')]
            
            nRows = channelOrParameter['rnum']
            nCols = channelOrParameter['cnum']
            if useNumpy:
                # Important note: Simulink or Etherlab seems to mess up rows and columns.
                # Therefore, a ROW_MAJOR matrix is actually a column-major matrix, and
                # the values for cnum (number of columns) and rnum (number of rows) are swapped.
                # The easiest way to cope with this is naively do what Simulink thinks and transpose
                # in the end.
                return np.array(v).reshape(nRows,nCols).transpose()
            else:
                # Return a list of lists.
                # Note that (as above), nCols and nRows are swapped, so we need to swap back
                # (use nCols here instead of nRows).
                return [ v[i::nCols] for i in range(nCols)]
        else: 
            raise RuntimeError("Unsupported channel type: %s"%typ)
        

    ## Parameter interfacing
    def _readParameters(self):
        # Get list of channels
        self._parameters = {}
        
        self._sock.send(b'<read_parameter/>')
        while True:
            tag = self._recvTag()
            if tag=='<parameters>':
                # Skip this opening tag
                continue
            elif tag.startswith('<parameter '):
                # Get info from this parameter
                
                # Extract the attributes from the XML data. We assume that all values are enclosed with "'s. (which seems to be the case always)
                attributes=re.findall('([a-zA-Z]+)="([^"]*)"',tag)
                parameter = Parameter([ (a[0], self._convertToNumberIfPossible(a[1])) for a in attributes])
                # Try to make numbers from 
                self._parameters[parameter['index']] = parameter
            elif tag=='</parameters>':
                # Then we're finished
                break
            elif tag.startswith('<pu '):
                # A parameter update notification. We can just ignore this one and contiune with the next tag.
                continue
            else:
                raise RuntimeError("Invalid tag encountered: ",tag)

    def parameterNames(self, asDict = False):
        """Returns a list of all parameter names. If asDict is True, it returns a dict
        of which the keys are the indices and the values the parameter names."""
        if not asDict:
            return [p['name'] for p in self._parameters.values()]
        else:
            return dict( [(p['index'],p['name']) for p in self._parameters.values()])                
    
    def parameterByName(self,s):
        """Returns the Parameter instance of the parameter whose name is s. Raises a ValueError if the parameter does not exist."""
        y = [ c for c in self._parameters.values() if c['name']==s]
        if len(y)==0:
            raise ValueError("No parameter with name",s)
        elif len(y)>1:
            raise RuntimeError("Multiple parameters with the same name (I thought that wasn't possible...")
        else:
            return y[0]
            
    def parameterIndexByName(self,s):
        """Returns the index of the parameter whose name is s."""
        return self.parameterByName(s)['index']
        
    def getParameterValue(self, p, useNumpy=None):
        """Given a Parameter instance, parameter index or parameter name, it returns the actual value of it.
        For scalars, it simply returns a scalar.
        For vectors and matrices, it returns numpy.array or list/list-of-lists (depending on
        the default state useNumpy, possibly overridden using the second argument of this function)."""
        
        if isinstance(p, Parameter) and 'index' in p.keys():
            index = p['index']
        elif isinstance(p,str):
            index = self.parameterIndexByName(p)
        elif isinstance(p,int):
            index = p
        else:
            raise ValueError("Not a valid identifier for a parameter (should be parameter name, index or Parameter instance): ",p)
        
        s = '<read_parameter index="%d"/>'%index
        self._sock.send(s.encode())
        
        while True:
            tag = self._recvTag()
            if tag.startswith('<parameter '):
                break
            # If we're here, we received a different tag, 
            # Most probably we obtained a ParameterUpdate (<pu xxx>)
            if self._debug:
                print("On getParameterValue, ignoring tag: ",tag)
            
        # Now tag is the one that we want
        attributes=re.findall('([a-zA-Z]+)="([^"]*)"',tag)
        data = dict([ (a[0], self._convertToNumberIfPossible(a[1])) for a in attributes])
        
        return self._stringToValue(data['value'], self._parameters[index])
                
    def setParameterValue(self, p, value):
        """Given a Parameter instance, parameter index or parameter name, and a value, this function
        sets the parameter value. As much as possible type checking is done to prevent errors. If a wrong
        value type is given, a ValueError is raised."""
        
        if isinstance(p, Parameter) and 'index' in p.keys():
            index = p['index']
        elif isinstance(p,str):
            index = self.parameterIndexByName(p)
        elif isinstance(p,int):
            index = p
        else:
            raise ValueError("Not a valid identifier for a parameter (should be parameter name, index or Parameter instance): ",p)
        
        valueAsString = self._valueToString(value, self._parameters[index])
        
        s = '<write_parameter index="%d" value="%s"/>'%(index,valueAsString)
        self._sock.send(s.encode())
        
        # We don't get a direct response to this (we do get ParameterUpdate notifications <pu >
        # though). We don't want to wait for any responses anyway; it's better to
        # just continue. If there are any <pu >'s, they will be discarded when we need
        # to read real important data (e.g., getParameterValue/getChannelValue).
        
    
            
    ## Channel (=signal) interfacing
    def _readChannels(self):
        # Get list of channels
        self._channels = {}
        
        self._sock.send(b'<read_kanaele/>')
        while True:
            tag = self._recvTag()
            if tag=='<channels>':
                # Skip this opening tag
                continue
            elif tag.startswith('<channel '):
                # Get info from this channel
                
                # Extract the attributes from the XML data. We assume that all values are enclosed with "'s. (which seems to be the case always)
                attributes=re.findall('([a-zA-Z]+)="([^"]*)"',tag)
                channel = Channel([ (a[0], self._convertToNumberIfPossible(a[1])) for a in attributes])
                # Try to make numbers from 
                self._channels[channel['index']] = channel
            elif tag=='</channels>':
                # Then we're finished
                break
            elif tag.startswith('<pu '):
                # A parameter update notification. We can just ignore this one and contiune with the next tag.
                continue
            else:
                raise RuntimeError("Invalid tag encountered: ",tag)

    def channelNames(self, asDict = False):
        """Returns a list of all channel names. If asDict is True, it returns a dict
        of which the keys are the indices and the values the channel names."""
        if not asDict:
            return [c['name'] for c in self._channels.values()]
        else:
            return dict( [(c['index'],c['name']) for c in self._channels.values()])
   
    def channelInstance(self, identifier):
        """Given a Channel identifier (an instance, channel index or channel name), it returns the channel instance."""
        if isinstance(identifier, Channel):
             return identifier
        elif isinstance(identifier,str): # A channel name was given
            y = [ c for c in self._channels.values() if c['name']==identifier]
            if len(y)==0:
                raise ValueError('No channel with name "%s"'%identifier)
            elif len(y)>1:
                raise RuntimeError('Multiple channels with the same name "%s" (I thought that wasn\'t possible...)'%identifier)
            else:
                return y[0]
        elif isinstance(identifier,int): # A channel index was given
            try:
                return self._channels[identifier]
            except KeyError:
                raise ValueError("No channel with index %i"%identifier)
        else:
            raise ValueError("Not a valid identifier for a channel (should be channel name, index or Channel instance): ",identifier)
    
    def channelIndex(self,c):
        """Given a Channel identifier (an instance, channel index or channel name), it returns the index of the channel."""
        return self.channelInstance(c)['index']
        
    def channelName(self,c):
        """Given a Channel identifier (an instance, channel index or channel name), it returns the name of the channel."""
        return self.channelInstance(c)['name']
    
            
    def getChannelValue(self,c, useNumpy=None):
        """Given a Channel instance, channel index or channel name, it returns the actual value of it.
        For scalars, it simply returns a scalar.
        For vectors and matrices, it returns numpy.array or list/list-of-lists (depending on
        the default state useNumpy, possibly overridden using the second argument of this function)."""
        
        index = self.channelIndex(c)
        
        s = '<read_kanaele index="%d"/>'%index
        self._sock.send(s.encode())
        
        while True:
            tag = self._recvTag()
            if tag.startswith('<channel '):
                break
            # If we're here, we received a different tag, 
            # Most probably we obtained a ParameterUpdate (<pu xxx>)
            if self._debug:
                print("On getChannelValue, ignoring tag: ",tag)
            
        # Now tag is the one that we want
        attributes=re.findall('([a-zA-Z]+)="([^"]*)"',tag)
        data = dict([ (a[0], self._convertToNumberIfPossible(a[1])) for a in attributes])
        
        return self._stringToValue(data['value'], self._channels[index], useNumpy)
    
## Test program  
if __name__=="__main__":  
    M=MSRClient('192.168.149.171')    
    print(M.parameterNames())
    print(M.channelNames())
    print(M.getParameterValue(0))
    print(M.getChannelValue(0))
        ##
    L= ChannelsLogger(M,blockSize = 200)
    L.setChannels([30,29,10,11,12,13,14])
    L._sock.settimeout(10)
    L.startLogging()
    
    
    
    
    
    
    
    