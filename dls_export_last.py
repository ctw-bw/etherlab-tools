#!/usr/bin/python3 
import sys,re,os,random
import string, shutil
import subprocess

PARAMETERS = {'tempDirName': '/tmp', 'tempExportDirPrefix': 'dls_export_'}
SCRIPTNAME = sys.argv[0].split("/")[-1]
HELP = SCRIPTNAME+""": a Python script which automatically exports the latest dls recording of a given job.

Usage:
dls_export_last <JobID> <filename.zip>

JobID should be the number of your DLS job. 
The data is exported to a temporary directory and compressed into a single
zip-file with the given name (the exported directory is removed afterwards).
"""


def run_command(cmd):
    """given shell command, returns the output of stdout and stderr"""
    y= subprocess.Popen(cmd, 
                            stdout=subprocess.PIPE, 
                            stderr=subprocess.PIPE, 
                            stdin=subprocess.PIPE).communicate()
    return y[1].decode()+y[0].decode() # Merge stderr and stdout into one string

def run_command_no_capture(cmd):
    """executes shell command, lets the command write to the default stdout (i.e., screen).
    Useful for commands that take long and have a progress bar"""
    return subprocess.Popen(cmd).wait()

def FindMostRecentChunck(dlsListOutput):
    """Given the output of 'dls list', it returns a string giving the start time of the most recent chunck."""
    
    # First, get a list of start dates
    startTimes = re.findall("Chunk from ([0-9]*)\.([0-9]*)\.([0-9]*) ([0-9]*):([0-9]*):([0-9]*)\.([0-9]*) to",dlsListOutput)
    
    # Now re-order them so that they are in 'decreasing significance'
    order = [2,1,0,3,4,5,6]
    startTimes = [ [s[i] for i in order] for s in startTimes ]
    
    # Finding most recent start time is easy. Formatting a strin gis as well:
    mostRecentTime = max(startTimes)
    return "-".join(mostRecentTime)

def AvailableSpace(path):
    """Returns the available space in bytes for the given path."""
    y = os.statvfs(path)
    return y.f_bsize * y.f_bavail
        
def CheckIfFileIsCanBeCreated(fileName):
    """This is done by trying to create a file and deleting it again. The file should not yet exist"""
    if os.path.exists(fileName):
        raise RuntimeError("File %s already exists in CheckIfFileCanBeCreated()"%fileName)
    try:
        f = open(fileName,'w')
    except Exception as E:
        print(E)
        print(type(E))
        return False
    f.close()
    os.remove(fileName)
    return True
        
        
def RepresentsInt(s):
    """Returns true if the string s represents an int"""
    try:
        int(s)
    except:
        return False
    return True
    
def ParseArguments():
    """Validates the arguments. Returns:
      - a dict of all arguments if ok, or
      - None if the number of arguments was incorrect (help should be displayed then),
      - a string containing an error message if the number of arguments was correct but any of the arguments didn't make sense.
    """
    if len(sys.argv) !=3:
        return None
    
    s = sys.argv[1] # First argument
    if s in ['--help', '-h', '-H']:
        return None # so that help is printed.
    elif not RepresentsInt(s):
        return "ERROR: First argument should be a job ID (an int). For help, run %s without arguments."%SCRIPTNAME
    else:
        jobID = int(s)
        
    # Parse second argument
    zipName = sys.argv[2]
    if len(zipName)>4 and zipName[-4:]==".zip":
        pass
    else:
        zipName = zipName+".zip"
        
    return {"jobID":jobID, "zipName": zipName}
    
def UniqueFileName(dir,prefix, randomLength = 5):
    """Returns a random file name which does not exist yet."""

    while True:
        fname = prefix+''.join(random.sample(string.ascii_lowercase,randomLength))
        if not os.path.exists(os.path.join(dir,fname)):
            return fname
        # else try again
        

## Function to improve the exported data
def ImproveExportedData(dirName):
    """Given a directory with exported data, this function 'improves' the data for more easy importing later.
    It does the following things:
    - Removes 'empty' files (i.e., channelX.dat's that only have a header and no data)
    - It makes sure that each file has the same number of samples and that they match each other
      (i.e., it trims all data which is not present in all channels).
    """
    def getNumberOfBytesOfNewline(s):
        """Given a line read from a file, ended with a newline, this function returns
        1 or 2, depending on the type of newline encoding. For "\r" or "\n" it returns
        1, for "\r\n" or "\n\r" it returns 2. If no newline encoding was found, it raises
        a ValueError."""
        if "\r\n" in s or "\n\r" in s:
            return 2
        elif "\r" in s or "\n" in s:
            return 1
        else:
            raise ValueError("The given string does not contain a newline character: *%s*"%s)
            
    # Get list of channel files
    fileNames = [ f for f in os.listdir(dirName) if re.match("channel[0-9]+\.dat", f) is not None ]
    
    if len(fileNames) == 0:
        raise RuntimeError("No channelXX.dat files found in directory %s",dirName)
    
    # Get information on each file: check whether it is 'empty' and remember the start and end time
    print("Getting start and end times of all channels")
    startTimes = []
    endTimes = []
    for fName in fileNames:
        with open(os.path.join(dirName, fName),'rb') as f:
            # 'Emtpy files' have 5 lines; f.readline() will return '' on the 6th call
            for i in range(6): l = f.readline().decode() # Get contents of 6th line (or '' if no 6th line exists)
            
            if l=='':
                # 'Empty file'
                startTimes.append(None)
                endTimes.append(None)
            else:
                startTimes.append(int(l.split("\t")[0])) # Extracts start time from data
                # Seek to somewhere before the end of the file and read all lines until the very end. 
                # The last line is the line we're looking for (if we had not enough bytes before the
                # end, we try again until we had enough.
                nBytesToRead = 0; ll=[]
                while len(ll)<2:
                    nBytesToRead += 100
                    f.seek(-nBytesToRead, os.SEEK_END) # go to nBytesToRead bytes before end of file
                    ll = [l.decode() for l in f.readlines()]
                    # We want to have at least two lines so that we're sure that we have the entire last line. 
                    # If we don't have that (which will probably never happen), we try again with more bytes
                endTimes.append(int(ll[-1].split("\t")[0]))
    # We now have a list of start/end times, which are None for 'empty' files
    maxStartTime = max([s for s in startTimes if s is not None])
    minEndTime = min([s for s in endTimes if s is not None])
    
    # We once again go over all files and do some stuff on each
    print("Adjusting start and end times so that they all match each other")
    for (fName, startTime, endTime) in zip(fileNames,startTimes,endTimes):
        if startTime is None:
            # Remove emtpy file
            print("Removing 'empty' data file %s."%fName)
            os.remove(os.path.join(dirName,fName))
        else:
            with open(os.path.join(dirName,fName),'r+b') as f: # r+b = (read and) write...
                print("Working on file %s"%fName)
                for i in range(5): l= f.readline() # Skip first 5 lines. Use the result of the last line to determine number of bytes of a newline (usually 1)
                newLineBytes = getNumberOfBytesOfNewline(l.decode())
                while True:
                    l = f.readline()
                    tme = int( l.decode().split("\t")[0]) # Now read the timestamp at the current line
                    if tme - maxStartTime<-10:
                        print("  removing line %s"%l)
                        # Then we need to throw away this sample. We use -10 instead of 0 because
                        # there somtimes are differences of 1 ns in the sample times
                        # Throwing away the data point is done by removing the preceding newline character and
                        # filling the line up with ***'s
                        # This way, we don't have to shorten (and therefore rewrite) the whole file
                        f.seek( - (len(l) + newLineBytes), os.SEEK_CUR)
                        f.write(b"*" * len(l)) # Overwrites the previous newline character(s) and the whole current line (without newline char(s))
                        f.seek(newLineBytes, os.SEEK_CUR) # Skip over newline char(s) to next line
                    else:
                        break
        
    
## Main script

# Parse arguments
arguments = ParseArguments()
if arguments is None:
    # Then no valid arguments were given
    print(HELP)
    exit(0)
elif isinstance(arguments,str):
    print(arguments)
    exit(0)
    
# Check if output file doesn't exist and if it can be created
if os.path.exists(arguments["zipName"]):
    print("ERROR: Output file %s already exists. Please remove file manually."%arguments["zipName"])
    exit(0)
if not CheckIfFileIsCanBeCreated(arguments["zipName"]):
    print("ERROR: Output file %s can not be created. Does the path exist? Do you have write access?"%arguments["zipName"])
    exit(0)
   
# Run the command to get the latest chunck   
dlsListOutput = run_command(['dls','list','-j',str(arguments["jobID"])])

# Check for any errors (don't know how yet)
if "No such job" in dlsListOutput:
    print("ERROR: Job number %d does not exist. Try 'dls list' for a list of job numbers."%arguments["jobID"])
    exit(0)
    
startTime = FindMostRecentChunck(dlsListOutput)

print("Timestamp of most recent recording of job %d: %s"%(arguments["jobID"],startTime),flush=True)

# Run the export command
tempDir = PARAMETERS['tempDirName']
tempExportDirName = UniqueFileName(tempDir, PARAMETERS['tempExportDirPrefix'])
fullTempDir = os.path.join(tempDir, tempExportDirName)
print("Exporting data to a temporary directory (%s)"%fullTempDir)
dlsExportOutput = run_command_no_capture(['dls','export','-j',str(arguments["jobID"]),'-o',tempDir,'-f',tempExportDirName,'-a','-s',startTime])

# Check whether there is still disk space (if not, then probably the export went wrong)
if AvailableSpace(fullTempDir)<1e6:
    print("""\nERROR: disk of temp dir (%s) is  full. Most probably, the export went wrong.
  In order to avoid incomplete data, we will not continue. Make sure that, after the export
  there is at least 1 MB free.
  
  Note that the temporary directory was not removed."""%tempDir)
    exit(0)
# Zipping into a single file
tempExportDirName = "dls_export_hedfr"
print("Zipping exported data into file %s"%arguments["zipName"])
run_command_no_capture(['zip','-r','-j',arguments["zipName"], fullTempDir])

# Remove temporary directory
print("Removing temporary directory %s"%fullTempDir)
# shutil.rmtree(fullTempDir)

